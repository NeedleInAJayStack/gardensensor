/*
 * Project GardenSensor
 * Author:Jay Herron 
 * Date: 2019-09-02
 * 
 * This code was written for web-connected garden temp/humidity sensors.
 * 
 * It implements reading/accessibility for a battery-connected Particle device that reads temp/humidity data from a Si7021 sensor.
 */

#include "Adafruit_Si7021.h"

static Adafruit_Si7021 si7021 = Adafruit_Si7021();
static double batteryVoltageCoeff = 0.0011224; // This is based on circuitry and found here: https://docs.particle.io/reference/device-os/firmware/xenon/#battery-voltage

// Record last-read and update intervals
long dataReadTime;
int dataInterval = 1; // in seconds

// Data variables
double temperature;
double humidity;
double batteryVoltage;


void setup() {
    Serial.begin(9600);

    // Declare particle variables
    Particle.variable("temperature", temperature);
    Particle.variable("humidity", humidity);
    Particle.variable("batteryVoltage", batteryVoltage);

    // Setup SI7021
    si7021.begin();

    // Set time intervals
    dataReadTime = Time.now();
}

void loop(void) {
    // Only read data on correct intervals
    if(Time.now() - dataReadTime > dataInterval) {
        
        // Read SI7021
        temperature = si7021.readTemperature();
        humidity = si7021.readHumidity();
        Serial.print("Temperature: "); Serial.print(temperature, 2); Serial.println("C");
        Serial.print("Humidity: "); Serial.print(humidity, 2); Serial.println("%RH");


        // Read battery level
        batteryVoltage = analogRead(BATT) * batteryVoltageCoeff;
        Serial.print("Battery Voltage: "); Serial.println(batteryVoltage);
        
        Serial.println("---");
        dataReadTime = Time.now();
    }
}