/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "application.h"
#line 1 "/home/jay/dev/particle/GardenSensor/src/GardenSensor.ino"
/*
 * Project GardenSensor
 * Author:Jay Herron 
 * Date: 2019-09-02
 * 
 */


// Photosensor
// Short pin <-> 3V3
// Long pin <-> A5, GND(through 220 Ohm resistor)

// Thermometer (holding the wires toward you, with the flat part on top)
// Left pin <-> GND
// Middle pin <-> D2, 3V3(through 4.7kOhm resistor)
// Right pin <-> 3V3

#include "DS18.h"

void setup();
void loop(void);
#line 20 "/home/jay/dev/particle/GardenSensor/src/GardenSensor.ino"
static int photoPin = A5;
static int tempPin = D2;
static int publishInterval = 60*1000; // In milliseconds
static int updateInterval = 1000; //In milliseconds
static float batteryVoltageCoeff = 0.0011224; // This is based on circuitry and found here: https://docs.particle.io/reference/device-os/firmware/xenon/#battery-voltage
long lastPublished;

static DS18 ds18 = DS18(tempPin);

int light;
double temperature;
float batteryVoltage;


void setup() {
    Serial.begin(9600);

    Particle.variable("light", light);
    Particle.variable("temperature", temperature);
}

void loop(void) {
    long time = millis();
    
    if(time - lastPublished >= publishInterval){
        Particle.publish("temperature", String(temperature), PRIVATE);
        Particle.publish("light", String(light), PRIVATE);
        Particle.publish("batteryVoltage", String(batteryVoltage), PRIVATE);
        lastPublished = millis();
    }

    // Read temp sensor
    if(ds18.read()){
        temperature = ds18.celsius();
        Serial.print("Temperature: "); Serial.println(temperature, 2);
    }

    // Read photosensor
    light = analogRead(photoPin);
    Serial.print("Light: "); Serial.println(light);

    // Read battery level
    batteryVoltage = analogRead(BATT) * batteryVoltageCoeff;
    Serial.print("Battery Voltage: "); Serial.println(batteryVoltage);


    Serial.println("---");
    //System.sleep(SLEEP_MODE_DEEP, sleepDuration, SLEEP_DISABLE_WKP_PIN);

    delay(updateInterval);
}