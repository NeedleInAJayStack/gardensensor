# Garden Sensor
The code contained here powers a Particle mesh network of nodes that collect environmental data useful for gardening.

## Gateway
The gateway is a [Particle Argon board](https://store.particle.io/products/argon-kit?_pos=2&_sid=9e70dd470&_ss=r) connected to a wifi router. This creates
the link between the mesh network and the Particle cloud.

## Nodes
The nodes are Particle Xenon boards, linked into the Argon's mesh network.

### Sensors
Each node contains a [Si7021 sensor](https://www.adafruit.com/product/3251) communicating via I2C, which reads temperature and humidity.
The particle boards have a built-in 

### Power
The nodes are solar-powered using the following equipment:

- [6V 1W Solar Panel](https://www.adafruit.com/product/3809)
- [USB / DC / Solar Lithium Ion/Polymer charger](https://www.adafruit.com/product/390)
- [Lithium Ion Cylindrical Battery - 3.7v 2200mAh](https://www.adafruit.com/product/1781)

### Construction
In order to weatherproof the sensor, it is placed in [an small enclosure](https://www.adafruit.com/product/903). A hole is drilled into the front of the
enclosure, the sensor wires are passed through and epoxyed, with the sensor remaining outside the enclosure. Around the sensor a 45-degree PVC pipe joint
is epoxyed. The enclosure is mounted onto a 4x4 post. The solar panel is glued to a 45-degree angle iron, which is also mounted to the post. Finally, a gland
is attached to the enclosure, the solar wires are routed through it, and connected to the board.

